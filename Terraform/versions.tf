locals {
    rancher_version = "v2.8.2"
    gitlab_agent_version = "v1.24.0"
    gitlab_runner_version = "v0.62.1"
    cert_manager_version = "v1.14.4"

    management_rke2_version = "v1.27.10+rke2r1"
    management_cert_manager_version = local.cert_manager_version
    management_gitlab_agent_version = local.gitlab_agent_version
    management_gitlab_runner_version = local.gitlab_runner_version
    
    staging_rke2_version = "v1.27.10+rke2r1"
    staging_gitlab_agent_version = local.gitlab_agent_version

    production_rke2_version = "v1.27.10+rke2r1"
    production_gitlab_agent_version = local.gitlab_agent_version

    develop_rke2_version = "v1.27.10+rke2r1"
    develop_gitlab_agent_version = local.gitlab_agent_version
}

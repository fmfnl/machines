# Obtain a token to register a GitLab Cluster Agent
# Ideally, the token is recycled whenever the VM is recreated, but
# this is currently not possible due to cyclicity issues.

resource "gitlab_cluster_agent" "develop-agent" {
    project = data.gitlab_project.current_project.id
    name = "develop"
}

resource "gitlab_cluster_agent_token" "develop-agent-token" {
    project = data.gitlab_project.current_project.id
    agent_id = gitlab_cluster_agent.develop-agent.agent_id
    name = "develop-token"
    description = "Token for the GitLab Runner on the Development cluster"

    lifecycle {
        replace_triggered_by = [
            gitlab_cluster_agent.develop-agent,
            terraform_data.cluster-develop-instance
        ]
    }
}

# We will also store this token in the Vault CI KV2 storage, so it can later
# be used by the helmfile pipelines for keeping the Cluster Agent up-to-date
resource "vault_kv_secret_v2" "develop-agent-token" {
    mount = "secrets-ci"
    name = "gitlab/agent-tokens/develop"
    data_json = jsonencode({
        token = gitlab_cluster_agent_token.develop-agent-token.token
    })
}

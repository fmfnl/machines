locals {
  production-rancher-flatcar-primary-ip = "129.125.36.6"
}

# Add cluster node
module "production-rancher-flatcar" {
    source = "./proxmox-flatcar-vm"

    replace_on = rancher2_cluster_v2.cluster-production-rancher.id
    depends_on = [
        cloudflare_record.production-rancher-internal
    ]

    provisioning_config = {
        ssh = merge(var.proxmox_ssh_config, { hostname = "elysium.fmf.nl" })
        additional_configs = [
            # Configure filesystem watch limit
            file("${path.module}/resources/unit-sysctl-watches.yaml"),

            # Deploy RKE2 with management integration
            templatefile("${path.module}/resources/unit-rke2-install-register.yaml", {
                command = "${rancher2_cluster_v2.cluster-production-rancher.cluster_registration_token.0.insecure_node_command} --etcd --controlplane --worker --node-name production-flatcar"
            }),

            # Install GitLab Cluster Agent within cluster
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "gitlab-agent.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-gitlab-agent.yaml", {
                    agent_token = gitlab_cluster_agent_token.production-agent-token.token
                    version = local.production_gitlab_agent_version
                })
            })
        ]
    }

    id = 302
    name = "prod-rancher"
    description = "Production cluster node running flatcar OS"
    node = "elysium"
    template = 502
    memory = {
        maximum = 16384
        minimum = 12288
    }
    core_count = 4

    networks = [
        {
            bridge_name = "public"
            address = "${local.production-rancher-flatcar-primary-ip}"
            range = "/32"
            dns = "1.1.1.1"
            extra = <<-EOT
                [Route]
                Gateway=10.0.0.1
                GatewayOnLink=yes
            EOT
        },
        {
            bridge_name = "intshare"
            address = "10.9.1.111"
            range = "/24"
        },
        {
            bridge_name = "intstor"
            address = "10.9.2.111"
            range = "/24"
        }
    ]

    drives = [{
        size = 64
        backing_storage = "vm_boot"
        interface = "scsi0"
    }]

    autostart = {
        timeout = 120
    }
}

# Add DNS registration
resource "cloudflare_record" "production-rancher-public" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "rancher.production"
    type = "CNAME"
    value = "rancher.fmf.nl"
}

# We call this "internal" as in, it's the address where we can reach rancher from the management
# server. However, in this case it corresponds to the public IP.
resource "cloudflare_record" "production-rancher-internal" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "rancher"
    type = "A"
    value = local.production-rancher-flatcar-primary-ip
}

# Write the DNS configuration to the Vault Kubernetes auth
resource "vault_kubernetes_auth_backend_config" "production-rancher-config" {
    backend = "kubernetes-production"
    kubernetes_host = cloudflare_record.production-rancher-internal.hostname
    kubernetes_ca_cert = "will be determined later but we have to enter it already"
}

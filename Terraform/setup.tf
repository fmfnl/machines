terraform {
    backend "http" {
    }
}

terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.49.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }

    rancher2 = {
      source = "rancher/rancher2"
      version = "1.25.0"
    }

    time = {
      source = "hashicorp/time"
      version = "0.9.1"
    }

    gitlab = {
      source = "gitlabhq/gitlab"
      version = "15.7.1"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.12.0"
    }
  }
}

provider "proxmox" {
  endpoint = var.proxmox_server
  insecure = var.proxmox_insecure

  username = var.proxmox_credentials.username
  password = var.proxmox_credentials.password

  #pm_api_token_id = var.proxmox_api_token
  #pm_api_token_secret = var.proxmox_api_secret

  ssh {
    agent = true
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "gitlab" {
  # Token will be automatically sourced from the proper environment variable
}

# This is the "seed" for the entire project; when this changes, the rancher-server
# will be recreated and thus all child clusters will be as well. TAKE CARE!!!
resource "random_uuid" "project-seed" {
}

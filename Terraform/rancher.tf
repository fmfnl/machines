# Rancher Management, which will need the rancher bootstrapper to complete
provider "rancher2" {
    insecure = true
    api_url = "https://management.internal.fmf.nl"

    token_key = rancher2_bootstrap.rancher-server-bootstrap.token
}

# Set up authentication/authorization through LDAP
resource "rancher2_auth_config_openldap" "rancher_auth_ldap" {
    lifecycle {
        replace_triggered_by = [
            rancher2_bootstrap.rancher-server-bootstrap
        ]
    }

    ## Server configuration
    servers = [ var.ldap_config.hostname ]
    port = 389
    
    ## Service Account configuration
    service_account_distinguished_name = var.ldap_config.username
    service_account_password = var.ldap_config.password
    test_username = var.ldap_config.testuser
    test_password = var.ldap_config.testpass

    ## Schema configuration
    # Users
    user_search_base = "ou=Members,dc=fmf,dc=nl"
    user_object_class = "x-FMFAccount"
    user_name_attribute = "cn"
    user_login_attribute = "uid"
    user_member_attribute = "memberOf"
    user_search_attribute = "uid|sn|givenName"

    # Groups
    group_search_base = "ou=Committees,dc=fmf,dc=nl"
    group_object_class = "x-FMFCommittee"
    group_name_attribute = "cn"
    group_dn_attribute = "dn"
    group_member_mapping_attribute = "member"
    group_member_user_attribute = "dn"
    group_search_attribute = "cn"
    nested_group_membership_enabled = false

    ## Access configuration
    access_mode = "restricted"
    allowed_principal_ids = [
        "openldap_group://cn=Comcie,ou=Committees,dc=fmf,dc=nl",
        "openldap_user://cn=Rancher Tester,ou=Members,dc=fmf,dc=nl"
    ]
}

# Give only Comcie access to the cluster
data "rancher2_global_role" "admin_role" {
    name = "Admin"
}

resource "rancher2_global_role_binding" "auth_comcie_admin" {
    lifecycle {
        replace_triggered_by = [
            rancher2_bootstrap.rancher-server-bootstrap
        ]
    }

    name = "comcie"
    global_role_id = data.rancher2_global_role.admin_role.id
    group_principal_id = "openldap_group://cn=Comcie,ou=Committees,dc=fmf,dc=nl"
}

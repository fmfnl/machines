locals {
  develop-rancher-flatcar-primary-ip = "129.125.36.8"
}

# Add cluster node
module "develop-rancher-flatcar" {
    source = "./proxmox-flatcar-vm"

    replace_on = rancher2_cluster_v2.cluster-develop-rancher.id
    depends_on = [
        cloudflare_record.develop-rancher-public
    ]

    provisioning_config = {
        ssh = merge(var.proxmox_ssh_config, { hostname = "delphi.fmf.nl" })
        additional_configs = [
            # Configure filesystem watch limit
            file("${path.module}/resources/unit-sysctl-watches.yaml"),

            # Deploy RKE2 with management integration
            templatefile("${path.module}/resources/unit-rke2-install-register.yaml", {
                command = "${rancher2_cluster_v2.cluster-develop-rancher.cluster_registration_token.0.insecure_node_command} --etcd --controlplane --worker --node-name production-flatcar"
            }),

            # Install GitLab Cluster Agent within cluster
            templatefile("${path.module}/resources/unit-rke2-crd.yaml", {
                name = "gitlab-agent.yaml"
                content = templatefile("${path.module}/resources/k8s-crd-gitlab-agent.yaml", {
                    agent_token = gitlab_cluster_agent_token.develop-agent-token.token
                    version = local.develop_gitlab_agent_version
                })
            })
        ]
    }

    id = 313
    name = "dev-rancher"
    description = "Development cluster node running flatcar OS"
    node = "delphi"
    template = 502
    memory = {
        maximum = 8192
        minimum = 6144
    }
    core_count = 4

    networks = [
        {
            bridge_name = "public"
            address = "${local.develop-rancher-flatcar-primary-ip}"
            range = "/32"
            dns = "1.1.1.1"
            extra = <<-EOT
                [Route]
                Gateway=10.0.0.1
                GatewayOnLink=yes
            EOT
        },
        {
            bridge_name = "intshare"
            address = "10.9.1.112"
            range = "/24"
        },
        {
            bridge_name = "intstor"
            address = "10.9.2.113"
            range = "/24"
        }
    ]

    drives = [{
        size = 64
        backing_storage = "vm_boot"
        interface = "scsi0"
    }]

    autostart = {
        timeout = 120
    }
}

# Add DNS registration
resource "cloudflare_record" "develop-rancher-public" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "rancher.develop"
    type = "A"
    value = local.develop-rancher-flatcar-primary-ip
}

resource "cloudflare_record" "develop-rancher-alias" {
    zone_id = data.cloudflare_zone.fmfnl.id

    name = "rancher.dev"
    type = "CNAME"
    value = "rancher.develop.fmf.nl"
}

# Write the DNS configuration to the Vault Kubernetes auth
resource "vault_kubernetes_auth_backend_config" "develop-rancher-config" {
    backend = "kubernetes-develop"
    kubernetes_host = cloudflare_record.develop-rancher-public.hostname
    kubernetes_ca_cert = "will be determined later but we have to enter it already"
}

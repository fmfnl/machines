# Building ignition config file from the templated Container Linux Config file
data "ct_config" "flatcar_config" {
    strict = true
    pretty_print = false

    content = templatefile("${path.module}/base_config_template.yaml", {
        hostname = local.vm_hostname,
        networks = { for i, network in var.networks: i => network },
        approle_roleid = data.vault_approle_auth_backend_role_id.approle_role.role_id
    })

    snippets = var.provisioning_config.additional_configs
}

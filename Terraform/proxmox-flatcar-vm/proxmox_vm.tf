# Adding a null resource that just serves as a means to upload the
# config file to Proxmox. It also asks the Proxmox server to generate
# a secret ID for the AppRole authentication of the VM. This way, no
# party has both parts of the VM credentials.
#
# This will run automatically if the flatcar config changes, but it won't
# run when only the VM is replaced (by hand for example). That is a bit
# unfortunate, but otherwise it would cause a loop. See also
# https://github.com/hashicorp/terraform/issues/31707
resource "null_resource" "config_provisioning" {
    # Only provision after the flatcar config is known
    depends_on = [
        data.ct_config.flatcar_config
    ]

    # Re-do the provisioning when the flatcar config has changed, but then
    # we do not yet replace the VM. We replace the VM when manually tainted
    # or when this input variable has changed.
    triggers = {
        content = sha1(data.ct_config.flatcar_config.rendered)
        input_observable = var.replace_on
    }

    # Specify SSH connection to Proxmox server
    connection {
        type = "ssh"

        host = var.provisioning_config.ssh.hostname
        user = var.provisioning_config.ssh.username
        
        agent = false
        private_key = local.private_key
        certificate = local.certificate
    }

    # Deploy the flatcar config file
    provisioner "file" {
        content = data.ct_config.flatcar_config.rendered
        destination = local.config_file
    }

    # Run the secret creation
    provisioner "local-exec" {
        command = join(" ", [ # Join is the only way to do multiline splitting of contiguous strings
                "vault write",

                # We want a wrapping token instead of a plaintext secret
                "-wrap-ttl=360s",
                "-field=wrapping_token",

                # Generated secret ID parameters
                "auth/approle/role/${var.approle_role}/secret-id",
                "secret_id_ttl=360s",

                # Bind the secret and later token to only be accessed by the VM
                join(" ", [for net in var.networks : "secret_id_bound_cidrs=${net.address}/32"]),
                join(" ", [for net in var.networks : "token_bound_cidrs=${net.address}/32"]),

                # Print the resulting token in the proper file on the server
                "> ${local.secret_filename}"
        ])
    }

    provisioner "file" {
        source = local.secret_filename
        destination = local.secret_file
    }
}

resource "proxmox_virtual_environment_vm" "flatcar_vm" {
    depends_on = [
        null_resource.config_provisioning,
        data.ct_config.flatcar_config
    ]

    # Replace the VM when this marker variable has changed value. This prevents
    # manual tainting and such.
    lifecycle {
       #replace_triggered_by = [ null_resource.config_provisioning ]
    }

    name = var.name
    vm_id = var.id
    node_name = var.node

    description = var.description
    kvm_arguments = join(" ", [
        "-fw_cfg name=opt/org.flatcar-linux/config,file=${local.config_file}",
        "-fw_cfg name=opt/nl.fmf.terraform/secrettoken,file=${local.secret_file}"
    ])

    clone {
        vm_id = var.template
        full = true
    }

    memory {
        dedicated = var.memory.maximum
        floating = var.memory.minimum == null ? var.memory.maximum : var.memory.minimum
    }

    cpu {
        cores = var.core_count
        type = "host"
    }

    dynamic "disk" {
        for_each = var.drives
        content {
            datastore_id = disk.value.backing_storage
            discard = "on"
            file_format = "raw"
            interface = disk.value.interface
            iothread = true
            size = disk.value.size
        }
    }

    migrate = true
    started = true

    scsi_hardware = "virtio-scsi-single"
    operating_system {
        type = "l26"
    }

    # Autostart config
    on_boot = var.autostart != null
    startup {
        order = var.autostart.order == null ? 0 : var.autostart.order
        up_delay = var.autostart.timeout
    }

    agent {
        enabled = true
        trim = true
    }

    dynamic "network_device" {
        for_each = var.networks
        content {
            model = "virtio"
            bridge = network_device.value.bridge_name
            firewall = true
            mtu = 1
        }
    }

    vga {
        enabled = true
        memory = 16
        type = "std"
    }

    timeout_create = 100
}

# A random UUID corresponding to a "deployment" of the VM. When the VM is recreated,
# the UUID will be regenerated, which can be used to redeploy some other resources
# outside of the module.
resource "random_uuid" "vm_instance" {
    keepers = {
        vm_id = proxmox_virtual_environment_vm.flatcar_vm.vm_id
    }
}

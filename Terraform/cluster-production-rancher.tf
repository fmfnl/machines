# Register Rancher cluster for production
resource "rancher2_cluster_v2" "cluster-production-rancher" {
    lifecycle {
        replace_triggered_by = [
            rancher2_bootstrap.rancher-server-bootstrap
        ]
    }
    
    # Basic cluster information
    name = "production"
    kubernetes_version = local.production_rke2_version
    default_cluster_role_for_project_members = "owner"

    # More detailed configuration
    enable_network_policy = false

    rke_config {
        ## Note that we should not increase the pod count to beyond 250 -
        ## this will cause networking issues so this should be thought about
        machine_global_config = <<EOF
            cni: "calico"
            kubelet-arg: [ "max-pods=250" ]
        EOF

        etcd {
            snapshot_schedule_cron = "0 */5 * * *"
            snapshot_retention = 5
        }

        chart_values = <<EOF
            rke2-ingress-nginx:
                controller:
                    allowSnippetAnnotations: true
        EOF
    }

    local_auth_endpoint {
        enabled = true
        fqdn = cloudflare_record.production-rancher-internal.hostname
    }
}

resource "terraform_data" "cluster-production-instance" {
    input = rancher2_cluster_v2.cluster-production-rancher.cluster_v1_id
}

output "cluster-production-registration-token" {
    value = rancher2_cluster_v2.cluster-production-rancher.cluster_registration_token
    sensitive = true
}

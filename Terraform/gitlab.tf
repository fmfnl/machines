data "gitlab_project" "current_project" {
    path_with_namespace = "fmfnl/infrastructure/kubernetes-clusters"
}

data "gitlab_group" "fmf_group" {
    full_path = "fmfnl"
}
